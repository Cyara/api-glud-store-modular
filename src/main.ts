import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, //elimina del payload todos los atributos que no estén definidas en el Data Transfers Objects
      forbidNonWhitelisted: true, //Alerta y no permite enviar el payload, cuando se están enviando atributos que no corresponden al DTO
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('API')
    .setDescription('GLUD STORE API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  // Habilitando CORS para despliegue de la aplicación en Heroku
  app.enableCors();

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
