import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { BrandsService } from '../services/brands.service';
import { CreateBrandDto, UpdateBrandDto } from '../dto/brand.dto';

@ApiTags('brands')
@Controller('brands')
export class BrandsController {
  /*@Post()
    create(@Body() payload:any){
        return{
            message: "Brands Post",
            payload,
        }
    }*/

  constructor(private brandService: BrandsService) {}

  @Get()
  findAll() {
    this.brandService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    this.brandService.findOne(id);
  }

  @Post()
  create(@Body() payload: CreateBrandDto) {
    this.brandService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payloadChange: UpdateBrandDto,
  ) {
    this.brandService.update(id, payloadChange);
  }

  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id: number) {
    this.brandService.remove(id);
  }
}
