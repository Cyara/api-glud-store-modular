import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  ParseIntPipe,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CategoriesService } from '../services/categories.service';
import { CreateCategoryDto, UpdatedCategoryDto } from '../dto/category.dto';

@ApiTags('categories')
@Controller('categories')
export class CategoriesController {
  constructor(private categoryService: CategoriesService) {}

  /*
    @Get('/:categoryId/products/:productId')
    getCategory(
                @Param('productId') productId:string,
                @Param('categoryId') categoryId:string
                )
    {
        return {message:`Product ${productId} and ${categoryId}`}
    }
    
    @Post()
    create(@Body() payload:any){
        return{
            message: "Categories Post",
            payload,
        }
    }*/

  @Get()
  findAll() {
    this.categoryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    this.categoryService.finOne(id);
  }

  @Post()
  create(@Body() payload: CreateCategoryDto) {
    this.categoryService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payloadChange: UpdatedCategoryDto,
  ) {
    this.categoryService.update(id, payloadChange);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    this.categoryService.delete(id);
  }
}
