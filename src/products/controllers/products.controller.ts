import {
  Controller,
  Get,
  Param,
  Post,
  Query,
  Body,
  Put,
  Delete,
  HttpCode,
  HttpStatus,
  Res,
  //ParseIntPipe
} from '@nestjs/common';
import { Response } from 'express';

import { ApiTags, ApiOperation } from '@nestjs/swagger';

import { ProductsService } from '../services/products.service';

//pipe creado
import { ParseIntPipe } from '../../common/parse-int.pipe';

//Data Transform Object
import { CreateProductDto, UpdateProductDto } from '../dto/products.dto';

@ApiTags('products')
@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  /** Rutas no dinámicas de primeras */
  @Get('/filter')
  getProductFilter() {
    return { message: `Soy un filtro` };
  }
  /** ./Rutas no dinámicas de primeras */

  /** Endpoint con Parametros */
  /*@Get('/:productId')
    getProduct(@Param() product:any){
        return {message:`Product ${product.productId}`}
    }*/

  @Get('/:productId')
  @HttpCode(HttpStatus.ACCEPTED)
  getProductId(@Param('productId', ParseIntPipe) productId: number) {
    //getProductId(@Res() response:Response, @Param('productId') productId:number){ // Estilo Express
    // Estilo Express
    /*response.status(200).send({
            message:`Product ${productId}`
        })*/
    //return {message:`Product ${productId}`}
    return this.productsService.findOne(productId);
  }
  /** ./Endpoint con Parametros */

  /** Endpoint con Parametros tipo Query*/

  @Get('')
  /*getProducts(@Query() params: any){
        const { limit, offset } = params;*/
  @ApiOperation({ summary: 'List of products' })
  getProducts(
    @Query('limit') limit: number,
    @Query('offset') offset = 0,
    @Query('brand') brand: string,
  ) {
    //const { limit, offset } = params; // destructurando
    //return {message:`products limit=${limit} & offset=${offset} & brand= ${brand}`};

    return this.productsService.findAll();
  }
  /** ./Endpoint con Parametros tipo Query*/

  @Post()
  create(@Body() payload: CreateProductDto) {
    /*return{
            message:'accion de crear',
            payload,
        }*/
    return this.productsService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateProductDto,
  ) {
    /*return{
            id,
            payload,
        }*/
    return this.productsService.update(id, payload);
  }

  @Delete(':id')
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.productsService.delete(id);
  }
}
