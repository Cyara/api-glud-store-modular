import {
  IsString,
  IsNumber,
  IsUrl,
  IsNotEmpty, //No puede ser vacío
  IsPositive,
} from 'class-validator';

//import { PartialType } from '@nestjs/mapped-types'
import { PartialType } from '@nestjs/swagger';

export class CreateProductDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;
  @IsString()
  @IsNotEmpty()
  readonly description: string;
  @IsUrl()
  @IsNotEmpty()
  readonly img: string;
  @IsNumber()
  @IsNotEmpty() //Obligatorio
  @IsPositive()
  readonly stock: number;
  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  readonly price: number;
}

// '?': indica variable opcional
/*
export class UpdateProductDto{
    readonly name?:string;
    readonly description?:string;
    readonly img?:string;
    readonly stock?:number;
    readonly price?:number;
}
*/

export class UpdateProductDto extends PartialType(CreateProductDto) {}
