import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBrandDto, UpdateBrandDto } from '../dto/brand.dto';
import { Brand } from '../../products/entities/brand.entity';

@Injectable()
export class BrandsService {
  private countId = 1;
  private brands: Brand[] = [
    {
      id: 1,
      name: 'Brand',
      image: 'https://ejemplo.image.com',
    },
    {
      id: 2,
      name: 'Brand',
      image: 'https://ejemplo.image.com',
    },
    {
      id: 3,
      name: 'Brand',
      image: 'https://ejemplo.image.com',
    },
  ];

  findAll() {
    return this.brands;
  }

  findOne(id: number) {
    const brandItem = this.brands.find((item) => item.id === id);
    if (!brandItem) {
      throw new NotFoundException(`Brand #${id} not found`);
    }
    return brandItem;
  }

  create(data: CreateBrandDto) {
    this.countId = this.countId + 1;
    const newBrand = {
      id: this.countId,
      ...data,
    };
    this.brands.push(newBrand);
    return newBrand;
  }

  update(id: number, payloadChange: UpdateBrandDto) {
    const brand = this.findOne(id);
    const index = this.brands.findIndex((el) => el.id === id);
    this.brands[index] = {
      ...brand,
      ...payloadChange,
    };
    return this.brands[index];
  }

  remove(id: number) {
    const index = this.brands.findIndex((el) => el.id === id);

    if (index === -1) {
      throw new NotFoundException(`Brand #${id} not found`);
    }
    this.brands.splice(index, 1);
    return true;
  }
}
