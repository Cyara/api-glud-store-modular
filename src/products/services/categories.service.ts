import { Injectable, NotFoundException } from '@nestjs/common';
import { Category } from '../entities/category.entity';
import { CreateCategoryDto, UpdatedCategoryDto } from '../dto/category.dto';

@Injectable()
export class CategoriesService {
  private countId = 1;
  private categories: Category[] = [
    {
      id: 1,
      name: 'category 1',
    },
    {
      id: 2,
      name: 'category 2',
    },
    {
      id: 3,
      name: 'category 3',
    },
  ];

  findAll() {
    return this.categories;
  }

  finOne(id: number) {
    const category = this.categories.find((el) => el.id === id);
    if (!category) {
      throw new NotFoundException(`Category ${id} not found`);
    }
    return category;
  }

  create(data: CreateCategoryDto) {
    this.countId += 1;
    const newCategory = {
      id: this.countId,
      ...data,
    };
    this.categories.push(newCategory);
    return newCategory;
  }

  update(id: number, payloadChange: UpdatedCategoryDto) {
    const category = this.finOne(id);
    const index = this.categories.findIndex((el) => el.id === id);
    this.categories[index] = {
      ...category,
      ...payloadChange,
    };
    return this.categories[index];
  }

  delete(id: number) {
    const index = this.categories.findIndex((el) => el.id === id);
    if (index === -1) {
      throw new NotFoundException(`Category ${id} not found`);
    }
    this.categories.splice(index, 1);
    return true;
  }
}
