import { Injectable, NotFoundException } from '@nestjs/common';
import { Product } from '../entities/product.entity';

//Data Transform Object
import { CreateProductDto, UpdateProductDto } from '../dto/products.dto';

@Injectable()
export class ProductsService {
  private counterId = 1;
  private products: Product[] = [
    {
      id: 1,
      name: 'product 1',
      description: 'lorem impsun',
      price: 122,
      stock: 10,
      img: 'https://www.google.com/es/',
    },
    {
      id: 2,
      name: 'product 2',
      description: 'lorem impsun',
      price: 122,
      stock: 10,
      img: 'https://www.google.com/es/',
    },
    {
      id: 3,
      name: 'product 3',
      description: 'lorem impsun',
      price: 122,
      stock: 10,
      img: 'https://www.google.com/es/',
    },
  ];

  findAll() {
    return this.products;
  }

  findOne(id: number) {
    const product = this.products.find((item) => item.id === id);
    if (!product) {
      //return null
      throw new NotFoundException(`Product #${id} not found`);
    }
    return product;
  }

  create(payload: CreateProductDto) {
    this.counterId += 1;
    const newProduct = {
      id: this.counterId,
      ...payload, //concatenar el payload
    };
    this.products.push(newProduct);
    return newProduct;
  }

  update(id: number, payload: UpdateProductDto) {
    const product = this.findOne(id);
    if (product) {
      const index = this.products.findIndex((item) => item.id === id);
      this.products[index] = {
        ...product,
        ...payload,
      };
      return this.products[index];
    }
    return null;
  }

  delete(id: number) {
    const index = this.products.findIndex((item) => item.id === id);
    if (index === -1) {
      throw new NotFoundException(`Product #${id} not found`);
    }
    this.products.splice(index, 1);
    return true;
  }
}
