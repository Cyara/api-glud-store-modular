import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CustomersService } from '../services/customers.service';
import { CreateCostumerDto, UpdateCostumerDto } from '../dto/customer.dto';

@ApiTags('customers')
@Controller('customers')
export class CustomersController {
  /*
        @Post()
        create(@Body() payload:any){
            return{
                message: "customers Post",
                payload,
            }
        }
    */
  constructor(private customersService: CustomersService) {}

  @Get()
  findAll() {
    this.customersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    this.customersService.findOne(id);
  }

  @Post()
  create(@Body() payload: CreateCostumerDto) {
    this.customersService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payloadChange: UpdateCostumerDto,
  ) {
    this.customersService.update(id, payloadChange);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    this.customersService.delete(id);
  }
}
