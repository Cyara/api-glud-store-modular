import { IsString, IsPhoneNumber, IsNotEmpty } from 'class-validator';
//import { PartialType } from '@nestjs/mapped-types'
import { PartialType } from '@nestjs/swagger';

export class CreateCostumerDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;
  @IsString()
  @IsNotEmpty()
  readonly lastname: string;
  @IsPhoneNumber()
  @IsNotEmpty()
  readonly phone: string;
}

export class UpdateCostumerDto extends PartialType(CreateCostumerDto) {}
