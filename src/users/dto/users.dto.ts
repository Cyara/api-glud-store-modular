import { IsString, IsEmail, Length, IsNotEmpty } from 'class-validator';
//import { PartialType } from '@nestjs/mapped-types'
import { PartialType, ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @IsString()
  @IsEmail()
  @ApiProperty({ description: 'the email of user' })
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  @Length(6)
  @ApiProperty({ description: 'the password of user' })
  readonly password: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'the rol of user' })
  readonly role: string;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}
