import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCostumerDto, UpdateCostumerDto } from '../dto/customer.dto';
import { Customer } from '../entities/customer.entity';

@Injectable()
export class CustomersService {
  private countId = 1;
  private customers: Customer[] = [
    {
      id: 1,
      name: 'Name Customer',
      lastname: 'Lastname customer',
      phone: '333 333333',
    },
    {
      id: 2,
      name: 'Name Customer',
      lastname: 'Lastname customer',
      phone: '333 333333',
    },
    {
      id: 3,
      name: 'Name Customer',
      lastname: 'Lastname customer',
      phone: '333 333333',
    },
  ];

  findAll() {
    return this.customers;
  }

  findOne(id: number) {
    const customer = this.customers.find((item) => item.id === id);
    if (!customer) {
      throw new NotFoundException(`Customer #${id} not found`);
    }
    return customer;
  }

  create(data: CreateCostumerDto) {
    this.countId = this.countId + 1;
    const newCustomer = {
      id: this.countId,
      ...data,
    };
    this.customers.push(newCustomer);
    return newCustomer;
  }

  update(id: number, change: UpdateCostumerDto) {
    const customer = this.findOne(id);
    const index = this.customers.findIndex((item) => item.id === id);
    if (index === -1) {
      throw new NotFoundException(`Customer #${id} not found`);
    }
    this.customers[index] = {
      ...customer,
      ...change,
    };
    return this.customers[index];
  }

  delete(id: number) {
    const index = this.customers.findIndex((item) => item.id === id);
    if (index === -1) {
      throw new NotFoundException(`Customer #${id} not found`);
    }
    this.customers.splice(index, 1);
    return true;
  }
}
