import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { User } from '../entities/user.entity';
import { Order } from '../entities/order.entity';

import { CreateUserDto, UpdateUserDto } from '../dto/users.dto';

import { ProductsService } from '../../products/services/products.service';

@Injectable()
export class UsersService {
  constructor(
    private productService: ProductsService,
    private configService: ConfigService,
  ) {}

  private counterId = 1;
  private users: User[] = [
    {
      id: 1,
      email: 'ejemplo@example.com',
      password: '12345',
      role: 'admin',
    },
    {
      id: 2,
      email: 'ejemplo@example.com',
      password: '12345',
      role: 'admin',
    },
    {
      id: 3,
      email: 'ejemplo@example.com',
      password: '12345',
      role: 'admin',
    },
  ];

  findAll() {
    const apikey = this.configService.get('API_KEY');
    const database = this.configService.get('DATABASE_NAME');
    console.log(`API_KEY: ${apikey}, DATABASE_NAME: ${database}`);
    return this.users;
  }

  findOne(id: number) {
    const user = this.users.find((el) => el.id === id);
    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }
    return user;
  }

  create(data: CreateUserDto) {
    this.counterId += 1;
    const newUser = {
      id: this.counterId,
      ...data,
    };
    this.users.push(newUser);
    return newUser;
  }

  update(id: number, change: UpdateUserDto) {
    const user = this.findOne(id);

    if (user) {
      const index = this.users.findIndex((el) => el.id === id);

      this.users[index] = {
        ...user,
        ...change,
      };
      return this.users[index];
    }
    return null;
  }

  delete(id: number) {
    const index = this.users.findIndex((el) => el.id === id);
    if (index === -1) {
      throw new NotFoundException(`User ${id} not found`);
    }
    this.users.splice(index, 1);
    return true;
  }

  getOrdersByUsers(id: number): Order {
    const user = this.findOne(id);
    return {
      date: new Date(),
      user,
      products: this.productService.findAll(),
    };
  }
}
